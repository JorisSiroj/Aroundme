package com.example.siroj.aroundme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ListPlaces extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_places);
    }

    //Launch map activity by cliking on the button
    public void _launchMap(View v){
        Intent it = new Intent(this, MapsActivity.class);
        startActivity(it);
    }
}
