package com.example.siroj.aroundme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siroj on 01/12/2017.
 */

public class DBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "aroundMe";
    // Contacts table name
    private static final String TABLE_BAR = "Bar";

    // Table Columns names
    private static final String GOOGLENAME = "googleName";
    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE "+this.TABLE_BAR+
                "(id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                this.GOOGLENAME+" TEXT,"+
                this.LONGITUDE+" REAL,"+
                this.LATITUDE+" REAL);";
        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + this.TABLE_BAR);
        // Creating tables again
        onCreate(db);
    }

    // Add some bar info like google name, longitude and latitude
    public void _AddBarInfo(Bar bar) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //Put the google name of the bar
        values.put(this.GOOGLENAME, bar.get_googleName());
        //put the longitude of the bar
        values.put(this.LONGITUDE, bar.get_longitude());
        //put the latitude of the bar
        values.put(this.LATITUDE, bar.get_latitude());
        // Inserting Row
        db.insert(this.TABLE_BAR, null, values);
        db.close();

    }

}
