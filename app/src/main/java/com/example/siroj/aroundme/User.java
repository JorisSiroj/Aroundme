package com.example.siroj.aroundme;

import android.content.Context;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

/**
 * Created by Siroj on 01/12/2017.
 */

//user = person which use the application
public class User {
    //user longitude
    private float _longitude;
    //user latitude
    private float _latitude;
    //Boolean for know if the geolocalization works
    private boolean _isDetected = false;
    //Boolean for know if the user is connect to internet
    private boolean _isConnected = false;

    public User() {
        //Should contain a condtion if the user is detect on the map, then the user is created
        //If the user is not located, we can create a default marker and use it at the place of the localization
        if(1==1){
            _isDetected = true;

            //Do a marker for localization
        } else {

        }

    }
    //If the bar is located within a radius of 2km return true, else return false
    public boolean _isAround2kmFromABar(Bar bar){

        return false;
    }

    //_numbermaxResult allows to put a maximum number of search, you can use -1 for have no limit.
    public List<Bar> _getAllBarRadius2Km(int _numberMaxResult){
        List<Bar> _myListOfBar = null;

        return _myListOfBar;
    }
    ///// GETTER AND SETTER /////
    public float get_longitude() {
        return _longitude;
    }

    public void set_longitude(float _longitude) {
        this._longitude = _longitude;
    }

    public float get_latitude() {
        return _latitude;
    }

    public void set_latitude(float _latitude) {
        this._latitude = _latitude;
    }

    public boolean isDetected() {
        return _isDetected;
    }

    public void setDetected(boolean detected) {
        _isDetected = detected;
    }
    ///// END OF GETTER AND SETTER /////
}
