package com.example.siroj.aroundme;

import android.widget.ImageView;

/**
 * Created by Siroj on 01/12/2017.
 */


public class Bar {

    //google name of the bar
    private String _googleName;
    //longitude of the bar
    private float _longitude;
    //latitude of the bar
    private float _latitude;
    //Photo of the bar
    private ImageView barPhoto;

    //Constructor
    public Bar(String _googleName, float _longitude, float _latitude) {
        this._googleName = _googleName;
        this._longitude = _longitude;
        this._latitude = _latitude;
    }


    ///// GETTER AND SETTER /////
    public String get_googleName() {
        return _googleName;
    }

    public void set_googleName(String _googleName) {
        this._googleName = _googleName;
    }

    public float get_longitude() {
        return _longitude;
    }

    public void set_longitude(float _longitude) {
        this._longitude = _longitude;
    }

    public float get_latitude() {
        return _latitude;
    }

    public void set_latitude(float _latitude) {
        this._latitude = _latitude;
    }

    public ImageView getBarPhoto() {
        return barPhoto;
    }

    public void setBarPhoto(ImageView barPhoto) {
        this.barPhoto = barPhoto;
    }

    ///// END OF GETTER AND SETTER /////
}
