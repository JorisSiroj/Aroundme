package com.example.siroj.aroundme;


import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //DIALOG PERMISSION
        int off = 0;

        try {
            //get the state of the actual permission
            off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        //If 0 permission is off so we launch a dialog
        if (off == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //Set the title of the dialog
            builder.setTitle(getString(R.string.msg_permission_title)); // All the string can be find in : app -> res -> values -> strings.xml
            //Set the message permission of the dialog
            builder.setMessage(getString(R.string.msg_permission));
            //Set the positive button of the dialog
            builder.setPositiveButton(getString(R.string.msg_permission_yes), new DialogInterface.OnClickListener() {

                //If the user click on yes, the setting location menu of his phone is display
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog
                    Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(onGPS);
                }
            });
            //If the user click no, the dialog is dismiss
            builder.setNegativeButton(getString(R.string.msg_permission_no), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Toulouse and move the camera
        LatLng toulouse = new LatLng(43.6, 1.4);
        mMap.addMarker(new MarkerOptions().position(toulouse).title("Marker in Toulouse"));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(toulouse));
        mMap.animateCamera(zoom);

        //I leave this part of code commented so that you see it, in a normal project I will have deleted it
/*
        //Display some results
        List<Address> myListOfBar = null;
        Geocoder geocoder = new Geocoder(this);
        try {
            //I set up for max 6 result, but we can change that number
            myListOfBar =  geocoder.getFromLocationName("bar", 6);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!myListOfBar.isEmpty()){
            //get the first result of the geocoder
            Address adress = myListOfBar.get(0);
            LatLng latlngOfTheAdress = new LatLng(adress.getLatitude(), adress.getLongitude());
            //Add a marker for the result
            mMap.addMarker(new MarkerOptions().position(latlngOfTheAdress).title("It's a bar, you can have fun"));
        }*/
    }
    //allow to launch the list view by clicking on the bouton
    public void _launchList(View v){
        Intent it = new Intent(this, ListPlaces.class);
        startActivity(it);
    }
}
